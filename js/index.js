//Codigo para activar los tooltips
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    //Velocidad de carrucel
    $(".carousel").carousel({
      interval:2000
    });
    //Evento show
    $('#contactopie').on('show.bs.modal', function(e){
      alert ("Inicio a abrirse");
    });
    //Evento show
    $('#contactopie').on('shown.bs.modal', function(e){
      alert ("Termino de abrise");
      $('#btncontacto').removeClass('btn-warning'); //remueve la clase
      $('#btncontacto').addClass('btn-primary'); //adiciona una clase
      $('#btncontacto').prop('disabled', true); //cambia la propiedad 
    });
    //Evento Hidden
    $('#contactopie').on('hide.bs.modal', function(e){
      alert ("Inicio a ocultarse");
    });
    //Evento Hidden
    $('#contactopie').on('hidden.bs.modal', function(e){
      alert ("Termino de ocultarse");
      $('#btncontacto').removeClass('btn-primary');
      $('#btncontacto').addClass('btn-warning');
      $('#btncontacto').prop('disabled', false);
    });
  });